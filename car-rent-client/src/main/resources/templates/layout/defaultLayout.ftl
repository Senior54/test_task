<#setting number_format="computer">
<#macro newLayout title="">
<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Client Car Rent</title>
	<link rel="stylesheet" href="/js/bootstrap/dist/css/bootstrap.css" type="text/css">
	<link rel="stylesheet" href="/js/bootstrap/dist/css/bootstrap-grid.css" type="text/css">
	<link rel="stylesheet" href="js/datepicker/css/bootstrap-datepicker.css" type="text/css">
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script src="/js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="/js/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script src="/js/datepicker/js/bootstrap-datepicker.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <#nested/>
</div>
</body>
</html>
</#macro>