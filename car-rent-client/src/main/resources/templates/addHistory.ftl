<#import "./layout/defaultLayout.ftl" as layout>
<@layout.newLayout>
<form class="form-signin" method="post" action="/addHistory">
	<h2 class="form-signin-heading">Add to history</h2>
	<p>
		<label for="carId" class="sr-only">Car</label>
		<select name="carRentId" class="form-select" aria-label="">
			<#list cars as car>
				<option value="${car.id}" selected>${car.name} ${car.number}</option>
            </#list>
		</select>
	</p>
	<p>
		<label for="pointFromId" class="sr-only">Point from</label>
		<select name="pointFromId" class="form-select" aria-label="">
            <#list points as point>
				<option value="${point.id}" selected>${point.city}, ${point.address}</option>
            </#list>
		</select>
	</p>
	<p>
		<label for="pointFinishId" class="sr-only">Point finish</label>
		<select name="pointFinishId" class="form-select" aria-label="">
            <#list points as point>
				<option value="${point.id}" selected>${point.city}, ${point.address}</option>
            </#list>
		</select>
	</p>
	<p>
		<label for="renterId" class="sr-only">Renter finish</label>
		<select name="renterId" class="form-select" aria-label="">
            <#list renters as renter>
				<option value="${renter.id}" selected>${renter.name}</option>
            </#list>
		</select>
	</p>

	<p>
	<label for="rentFrom" class="sr-only">Start time of rent</label>
	<input class="form-select" name="rentFrom" data-provide="datepicker" data-date-format="yyyy-mm-dd">
	</p>

	<p>
	<label for="rentFinish" class="sr-only">Start time of rent</label>
	<input class="form-select" name="rentFinish" data-provide="datepicker" data-date-format="yyyy-mm-dd">
	</p>

	<button class="btn btn-lg btn-primary btn-block" type="submit"><#if !history??>Create<#else>Edit</#if></button>
</form>
	<script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
	</script>
</@layout.newLayout>