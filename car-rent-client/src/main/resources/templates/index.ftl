<#import "./layout/defaultLayout.ftl" as layout>
<@layout.newLayout>
	<a href="/addHistory" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Create history</a>
	<table class="table">
		<tr>
			<th>Car model</th>
			<th>Point from</th>
			<th>Average days at point</th>
			<th>Point finish</th>
			<th>Renter full name</th>
			<th>Start time of rent</th>
			<th>Finish time of rent</th>
		</tr>
        <#list page.content as content>
			<tr>
				<td><a href="/?carId=${content.car.id}">${content.car.name} ${content.car.number}</a></td>
				<td>${content.pointFrom.city}, ${content.pointFrom.address}</td>
				<td>${content.counter.average}</td>
				<td>${content.pointFinish.city}, ${content.pointFinish.address}</td>
				<td>${content.renter.name}</td>
				<td>${content.rentFrom}</td>
				<td>${content.rentFinish}</td>
			</tr>
        </#list>
	</table>
    <#import "./layout/pager.ftl" as p>
    <@p.pager page url />
</@layout.newLayout>