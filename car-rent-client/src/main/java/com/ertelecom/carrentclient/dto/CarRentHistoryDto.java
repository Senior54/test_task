package com.ertelecom.carrentclient.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CarRentHistoryDto {
	private Long id;
	private Long carRentId;
	private Long pointFromId;
	private Long pointFinishId;
	private Long renterId;

	private String rentFrom;
	private String rentFinish;
}