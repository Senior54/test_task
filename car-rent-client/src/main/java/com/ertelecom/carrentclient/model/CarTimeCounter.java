package com.ertelecom.carrentclient.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarTimeCounter {
	private Car car;

	private Point point;

	private Long counter;
	private Long days;

	public Double getAverage() {
		return (double) days / counter;
	}
}
