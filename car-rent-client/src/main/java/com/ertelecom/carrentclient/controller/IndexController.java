package com.ertelecom.carrentclient.controller;

import com.ertelecom.carrentclient.dto.CarRentHistoryDto;
import com.ertelecom.carrentclient.helper.RestPage;
import com.ertelecom.carrentclient.model.Car;
import com.ertelecom.carrentclient.model.CarRentHistory;
import com.ertelecom.carrentclient.model.Point;
import com.ertelecom.carrentclient.model.Renter;
import com.ertelecom.carrentclient.util.JacksonUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
public class IndexController {

	private final String urlRest;

	public IndexController(EurekaClient eurekaClient) {

		InstanceInfo service = eurekaClient
				.getApplication("Car Rent")
				.getInstances()
				.get(0);

		String hostName = service.getHostName();
		int port = service.getPort();

		this.urlRest = "http://" + hostName + ":" + port;
	}

	@GetMapping({"/"})
	public String index(Model model,
						@RequestParam(defaultValue = "0") long carId,
						@RequestParam(defaultValue = "0") int page,
						@RequestParam(defaultValue = "10") int size) throws JsonProcessingException {

		RestTemplate restTemplate = new RestTemplate();
		String response = restTemplate.getForEntity(this.urlRest + "/api/history/getPages?page=" + page + "&size=" + size + "&carId=" + carId, String.class).getBody();
		RestPage<CarRentHistory> history = JacksonUtil.getMapper().readValue(response, new TypeReference<>() {
		});
		model.addAttribute("page", history);
		model.addAttribute("url", "/?carId=" + carId);

		return "index";
	}

	@GetMapping("/addHistory")
	public String addHistory(@RequestParam(defaultValue = "0") long historyId,
							 Model model) throws JsonProcessingException {

		RestTemplate restTemplate = new RestTemplate();
		String response = restTemplate.getForEntity(this.urlRest + "/api/cars/getAll", String.class).getBody();
		List<Car> cars = JacksonUtil.getMapper().readValue(response, new TypeReference<>() {
		});

		model.addAttribute("cars", cars);

		response = restTemplate.getForEntity(this.urlRest + "/api/points/getAll", String.class).getBody();
		List<Point> points = JacksonUtil.getMapper().readValue(response, new TypeReference<>() {
		});

		model.addAttribute("points", points);

		response = restTemplate.getForEntity(this.urlRest + "/api/renters/getAll", String.class).getBody();
		List<Renter> renters = JacksonUtil.getMapper().readValue(response, new TypeReference<>() {
		});

		model.addAttribute("renters", renters);

		if(historyId != 0) {
			response = restTemplate.getForEntity(this.urlRest + "/api/history/get/" + historyId, String.class).getBody();
			List<Renter> history = JacksonUtil.getMapper().readValue(response, new TypeReference<>() {
			});

			model.addAttribute("history", history);
		} else {
			model.addAttribute("history", null);
		}


		return "addHistory";
	}

	@PostMapping("/addHistory")
	public String addHistory(CarRentHistoryDto carRentHistory) {
		RestTemplate restTemplate = new RestTemplate();

		HttpEntity<CarRentHistoryDto> request = new HttpEntity<>(carRentHistory);
		restTemplate.postForObject(this.urlRest + "/api/history/create", request, CarRentHistoryDto.class);

		return "redirect:/";
	}
}
