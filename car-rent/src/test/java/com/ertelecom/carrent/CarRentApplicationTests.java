package com.ertelecom.carrent;

import com.ertelecom.carrent.model.*;
import com.ertelecom.carrent.service.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Slf4j
@SpringBootTest
@ComponentScan({"com.ertelecom.carrent"})
class CarRentApplicationTests {

	@Autowired
	private CarService carService;

	@Autowired
	private PointService pointService;

	@Autowired
	private RenterService renterService;

	@Autowired
	private CarRentHistoryService carRentHistoryService;

	@Test
	void contextLoads() {
	}


	@Test
	@Transactional
	public void testHistory() {
		Car car = new Car("Toyota", "O666OO99");
		carService.create(car);
		log.info("car was created : {}", car);
		log.info("------------------");

		Point pointFrom = new Point("Moscow", "Lenina 1");
		pointService.create(pointFrom);
		log.info("point start was created : {}", pointFrom);
		log.info("------------------");

		Point pointFinish = new Point("Moscow", "Lenina 1");
		pointService.create(pointFinish);
		log.info("point finish created : {}", pointFinish);
		log.info("------------------");

		Renter renter = new Renter("Test User Name");
		renterService.create(renter);
		log.info("renter was created : {}", renter);
		log.info("------------------");

		CarRentHistory carRentHistory = new CarRentHistory(car, pointFrom, pointFinish, renter, LocalDate.now(), LocalDate.now().plusDays(1));
		carRentHistoryService.create(carRentHistory);

		log.info("renter was history created : {}", carRentHistory);
		log.info("------------------");

		carService.delete(car.getId());
		log.info("car was deleted : {}", car);

		pointService.delete(pointFrom.getId());
		log.info("point was deleted : {}", pointFrom);

		renterService.delete(renter.getId());
		log.info("renter was deleted : {}", renter);
	}
}
