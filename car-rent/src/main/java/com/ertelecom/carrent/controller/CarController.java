package com.ertelecom.carrent.controller;

import com.ertelecom.carrent.model.Car;
import com.ertelecom.carrent.service.CarService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/cars")
public class CarController {

	private final CarService carService;

	public CarController(CarService carService) {
		this.carService = carService;
	}

	@ApiOperation(value = "Get car by id")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	public Car get(@PathVariable Long id) {
		return carService.get(id);
	}


	@ApiOperation(value = "Get all cars")
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public Iterable<Car> getAllCars() {
		return carService.getAll();
	}

	@ApiOperation(value = "Create car")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public Car addCar(@Valid @RequestBody Car car) {
		return carService.create(car);
	}

	@ApiOperation(value = "Delete car")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public ResponseEntity<String> removeCar(@PathVariable Long id) {
		carService.delete(id);
		return ResponseEntity.ok().build();
	}

	@ApiOperation(value = "Edit car")
	@RequestMapping(value = "/updateCar", method = RequestMethod.POST)
	public Car editCar(@Valid @RequestBody Car car) {
		return carService.update(car);
	}
}
