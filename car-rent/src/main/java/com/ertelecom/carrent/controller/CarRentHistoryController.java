package com.ertelecom.carrent.controller;

import com.ertelecom.carrent.dto.CarRentHistoryDto;
import com.ertelecom.carrent.helper.RestPage;
import com.ertelecom.carrent.model.CarRentHistory;
import com.ertelecom.carrent.service.CarRentHistoryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/history")
public class CarRentHistoryController {

	private final CarRentHistoryService carRentHistoryService;

	public CarRentHistoryController(CarRentHistoryService carRentHistoryService) {
		this.carRentHistoryService = carRentHistoryService;
	}

	@ApiOperation(value = "Get history by id")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	public CarRentHistory get(@PathVariable Long id) {
		return carRentHistoryService.get(id);
	}

	@RequestMapping(value = "/getPages", method = RequestMethod.GET)
	public ResponseEntity<RestPage<CarRentHistory>> getPages(
			@RequestParam(defaultValue = "0") long carId,
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "10") int size
	) {
		Pageable paging = PageRequest.of(page, size);

		Page<CarRentHistory> history;

		if(carId == 0) {
			history = carRentHistoryService.getPages(paging);
		} else {
			history = carRentHistoryService.getPagesFilterCarId(paging, carId);
		}

		RestPage<CarRentHistory> pagination = new RestPage<CarRentHistory>();

		pagination.setContent(history.getContent());
		pagination.setNumber(history.getNumber());
		pagination.setTotalPages(history.getTotalPages());
		pagination.setTotalItems(history.getTotalElements());
		pagination.setSize(history.getSize());

		return new ResponseEntity<>(pagination, HttpStatus.OK);
	}

	@ApiOperation(value = "Create history")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public CarRentHistory addCarRentHistory(@Valid @RequestBody CarRentHistoryDto carRentHistory) {
		return carRentHistoryService.create(carRentHistory);
	}

	@ApiOperation(value = "Delete history")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public ResponseEntity<String> removeCarRentHistory(@PathVariable Long id) {
		carRentHistoryService.delete(id);
		return ResponseEntity.ok().build();
	}

	@ApiOperation(value = "Edit history")
	@RequestMapping(value = "/updateCarRentHistory", method = RequestMethod.POST)
	public CarRentHistory editCarRentHistory(@Valid @RequestBody CarRentHistoryDto carRentHistory) {
		return carRentHistoryService.update(carRentHistory);
	}
}
