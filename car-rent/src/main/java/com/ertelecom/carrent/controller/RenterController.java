package com.ertelecom.carrent.controller;

import com.ertelecom.carrent.model.Renter;
import com.ertelecom.carrent.service.RenterService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/renters")
public class RenterController {

	private final RenterService renterService;

	public RenterController(RenterService renterService) {
		this.renterService = renterService;
	}

	@ApiOperation(value = "Get renter by id")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	public Renter get(@PathVariable Long id) {
		return renterService.get(id);
	}

	@ApiOperation(value = "Get all renters")
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public Iterable<Renter> getAllRenters() {
		return renterService.getAll();
	}

	@ApiOperation(value = "Create renter")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public Renter addRenter(@Valid @RequestBody Renter renter) {
		return renterService.create(renter);
	}

	@ApiOperation(value = "Delete renter")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public ResponseEntity<String> removeRenter(@PathVariable Long id) {
		renterService.delete(id);
		return ResponseEntity.ok().build();
	}

	@ApiOperation(value = "Edit renter")
	@RequestMapping(value = "/updateRenter", method = RequestMethod.POST)
	public Renter editRenter(@Valid @RequestBody Renter renter) {
		return renterService.update(renter);
	}
}
