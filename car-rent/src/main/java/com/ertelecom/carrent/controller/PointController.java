package com.ertelecom.pointrent.controller;

import com.ertelecom.carrent.model.Point;
import com.ertelecom.carrent.service.PointService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/points")
public class PointController {

	private final PointService pointService;

	public PointController(PointService pointService) {
		this.pointService = pointService;
	}

	@ApiOperation(value = "Get point by id")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	public Point get(@PathVariable Long id) {
		return pointService.get(id);
	}

	@ApiOperation(value = "Get all points")
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public Iterable<Point> getAllPoints() {
		return pointService.getAll();
	}

	@ApiOperation(value = "Create point")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public Point addPoint(@Valid @RequestBody Point point) {
		return pointService.create(point);
	}

	@ApiOperation(value = "Delete point")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public ResponseEntity<String> removePoint(@PathVariable Long id) {
		pointService.delete(id);
		return ResponseEntity.ok().build();
	}

	@ApiOperation(value = "Edit point")
	@RequestMapping(value = "/updatePoint", method = RequestMethod.POST)
	public Point editPoint(@Valid @RequestBody Point point) {
		return pointService.update(point);
	}
}
