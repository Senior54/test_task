package com.ertelecom.carrent.helper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RestPage<T> {
	private List<T> content;
	private int number;
	private long totalItems;
	private int totalPages;
	private int size;
}
