package com.ertelecom.carrent.repositories;

import com.ertelecom.carrent.model.Car;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends PagingAndSortingRepository<Car, Long> {

}
