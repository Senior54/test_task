package com.ertelecom.carrent.repositories;

import com.ertelecom.carrent.model.Car;
import com.ertelecom.carrent.model.CarRentHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRentHistoryRepository extends PagingAndSortingRepository<CarRentHistory, Long> {
	Page<CarRentHistory> findAllByCar(Pageable pageable, Car car);
}
