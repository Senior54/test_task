package com.ertelecom.carrent.repositories;

import com.ertelecom.carrent.model.Point;
import org.springframework.stereotype.Repository;

@Repository
public interface PointRepository extends PagingAndSortingRepository<Point, Long> {

}
