package com.ertelecom.carrent.repositories;

import com.ertelecom.carrent.model.Car;
import com.ertelecom.carrent.model.CarTimeCounter;
import com.ertelecom.carrent.model.Point;
import org.springframework.stereotype.Repository;

@Repository
public interface CarTimeCounterRepository extends PagingAndSortingRepository<CarTimeCounter, Long> {
	CarTimeCounter findCarTimeCounterByCarAndPoint(Car car, Point point);
}
