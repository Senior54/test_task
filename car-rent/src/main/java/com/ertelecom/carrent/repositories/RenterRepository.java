package com.ertelecom.carrent.repositories;

import com.ertelecom.carrent.model.Renter;
import org.springframework.stereotype.Repository;

@Repository
public interface RenterRepository extends PagingAndSortingRepository<Renter, Long> {

}
