package com.ertelecom.carrent.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cars")
public class Car extends AbstractId {
	@NotBlank(message = "Name is mandatory")
	@ApiModelProperty(notes = "Car Model")
	private String name;

	@NotBlank(message = "Number is mandatory")
	@ApiModelProperty(notes = "Car number")
	private String number;
}
