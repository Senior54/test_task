package com.ertelecom.carrent.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "renters")
public class Renter extends AbstractId {
	@NotBlank(message = "Full name is mandatory")
	@ApiModelProperty(notes = "Full name")
	private String name;
}
