package com.ertelecom.carrent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cars_rent_history")
public class CarRentHistory extends AbstractId {
	@ApiModelProperty(notes = "Rented car")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "car_id", referencedColumnName = "id")
	private Car car;

	@ApiModelProperty(notes = "From point")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "point_from_id", referencedColumnName = "id")
	private Point pointFrom;

	@ApiModelProperty(notes = "Finish point")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "point_finish_id", referencedColumnName = "id")
	private Point pointFinish;

	@ApiModelProperty(notes = "Renter")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "renter_id", referencedColumnName = "id")
	private Renter renter;

	@ApiModelProperty(notes = "Time of start the rent", example = "yyyy-MM-dd", value = "0000-00-00")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate rentFrom;

	@ApiModelProperty(notes = "Time of finish the rent", example = "yyyy-MM-dd", value = "0000-00-00")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate rentFinish;

	@ApiModelProperty(notes = "Car counter time")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "counter_id", referencedColumnName = "id")
	private CarTimeCounter counter;

	public CarRentHistory(Car car, Point pointFrom, Point pointFinish, Renter renter, LocalDate rentFrom, LocalDate rentFinish) {
		this.car = car;
		this.pointFrom = pointFrom;
		this.pointFinish = pointFinish;
		this.renter = renter;
		this.rentFrom = rentFrom;
		this.rentFinish = rentFinish;
	}
}
