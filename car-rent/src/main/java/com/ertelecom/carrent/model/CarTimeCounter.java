package com.ertelecom.carrent.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "car_time_counters")
public class CarTimeCounter extends AbstractId {
	@ApiModelProperty(notes = "Rented car")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "car_id", referencedColumnName = "id")
	private Car car;

	@ApiModelProperty(notes = "From point")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "point_id", referencedColumnName = "id")
	private Point point;

	private Long counter;
	private Long days;

	public Double getAverage() {
		return (double) days / counter;
	}
}
