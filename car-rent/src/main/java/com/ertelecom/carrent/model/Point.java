package com.ertelecom.carrent.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "points")
public class Point extends AbstractId {
	@NotBlank(message = "City is mandatory")
	@ApiModelProperty(notes = "City")
	private String city;

	@NotBlank(message = "Address is mandatory")
	@ApiModelProperty(notes = "Address")
	private String address;
}
