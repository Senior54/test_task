package com.ertelecom.carrent.service;

import com.ertelecom.carrent.model.Car;
import com.ertelecom.carrent.model.CarTimeCounter;
import com.ertelecom.carrent.model.Point;
import org.springframework.stereotype.Service;

@Service
public interface CarTimeCounterService extends AbstractCrud<CarTimeCounter> {
	CarTimeCounter getCarAndPoint(Car car, Point point);
}
