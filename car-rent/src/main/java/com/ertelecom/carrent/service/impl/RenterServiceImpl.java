package com.ertelecom.carrent.service.impl;

import com.ertelecom.carrent.model.Renter;
import com.ertelecom.carrent.repositories.RenterRepository;
import com.ertelecom.carrent.service.RenterService;
import org.springframework.stereotype.Service;

@Service
public class RenterServiceImpl extends CrudImpl<RenterRepository, Renter> implements RenterService {

	public RenterServiceImpl(RenterRepository repository) {
		super(repository);
	}
}
