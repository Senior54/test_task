package com.ertelecom.carrent.service;

import com.ertelecom.carrent.model.Car;
import org.springframework.stereotype.Service;

@Service
public interface CarService extends AbstractCrud<Car> {

}
