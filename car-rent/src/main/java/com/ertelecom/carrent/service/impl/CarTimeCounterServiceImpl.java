package com.ertelecom.carrent.service.impl;

import com.ertelecom.carrent.model.Car;
import com.ertelecom.carrent.model.CarTimeCounter;
import com.ertelecom.carrent.model.Point;
import com.ertelecom.carrent.repositories.CarTimeCounterRepository;
import com.ertelecom.carrent.service.CarTimeCounterService;
import org.springframework.stereotype.Service;

@Service
public class CarTimeCounterServiceImpl extends CrudImpl<CarTimeCounterRepository, CarTimeCounter> implements CarTimeCounterService {

	public CarTimeCounterServiceImpl(CarTimeCounterRepository repository) {
		super(repository);
	}

	@Override
	public CarTimeCounter getCarAndPoint(Car car, Point point) {
		return getRepository().findCarTimeCounterByCarAndPoint(car, point);
	}
}
