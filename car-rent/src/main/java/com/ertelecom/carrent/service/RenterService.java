package com.ertelecom.carrent.service;

import com.ertelecom.carrent.model.Renter;
import org.springframework.stereotype.Service;

@Service
public interface RenterService extends AbstractCrud<Renter> {
}
