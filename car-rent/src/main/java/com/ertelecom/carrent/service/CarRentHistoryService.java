package com.ertelecom.carrent.service;

import com.ertelecom.carrent.dto.CarRentHistoryDto;
import com.ertelecom.carrent.model.CarRentHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface CarRentHistoryService extends AbstractCrud<CarRentHistory> {
	CarRentHistory create(CarRentHistoryDto carRentHistoryDto);

	CarRentHistory update(CarRentHistoryDto carRentHistoryDto);

	Page<CarRentHistory> getPagesFilterCarId(Pageable pageable, Long carId);
}
