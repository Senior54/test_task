package com.ertelecom.carrent.service.impl;

import com.ertelecom.carrent.model.Point;
import com.ertelecom.carrent.repositories.PointRepository;
import com.ertelecom.carrent.service.PointService;
import org.springframework.stereotype.Service;

@Service
public class PointServiceImpl extends CrudImpl<PointRepository, Point> implements PointService {

	public PointServiceImpl(PointRepository repository) {
		super(repository);
	}
}
