package com.ertelecom.carrent.service.impl;

import com.ertelecom.carrent.model.AbstractId;
import com.ertelecom.carrent.repositories.PagingAndSortingRepository;
import com.ertelecom.carrent.service.AbstractCrud;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public abstract class CrudImpl<T extends PagingAndSortingRepository<S, Long>, S extends AbstractId> implements AbstractCrud<S> {
	private final T repository;

	public CrudImpl(T repository) {
		this.repository = repository;
	}

	public S get(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("Did not find by id " + id));
	}

	public List<S> getAll() {
		return repository.findAll();
	}

	@Override
	public Page<S> getPages(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public S create(S object) {
		return repository.save(object);
	}

	public S update(S object) {
		return repository.save(object);
	}

	public void delete(Long id) {
		repository.deleteById(id);
	}

	protected T getRepository() {
		return repository;
	}
}
