package com.ertelecom.carrent.service;

import com.ertelecom.carrent.model.AbstractId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AbstractCrud<T extends AbstractId> {
	T get(Long id);

	List<T> getAll();

	Page<T> getPages(Pageable pageable);

	T create(T object);

	T update(T object);

	void delete(Long id);
}
