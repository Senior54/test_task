package com.ertelecom.carrent.service.impl;

import com.ertelecom.carrent.dto.CarRentHistoryDto;
import com.ertelecom.carrent.model.*;
import com.ertelecom.carrent.repositories.CarRentHistoryRepository;
import com.ertelecom.carrent.service.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class CarRentHistoryServiceImpl extends CrudImpl<CarRentHistoryRepository, CarRentHistory> implements CarRentHistoryService {

	private final CarService carService;

	private final RenterService renterService;

	private final PointService pointService;

	private final CarTimeCounterService carTimeCounterService;

	public CarRentHistoryServiceImpl(CarRentHistoryRepository repository, CarService carService, RenterService renterService, PointService pointService, CarTimeCounterService carTimeCounterService) {
		super(repository);
		this.carService = carService;
		this.renterService = renterService;
		this.pointService = pointService;
		this.carTimeCounterService = carTimeCounterService;
	}


	public Page<CarRentHistory> getPagesFilterCarId(Pageable pageable, Long carId) {
		return getRepository().findAllByCar(pageable, carService.get(carId));
	}

	@Transactional
	public CarRentHistory create(CarRentHistoryDto carRentHistoryDto) {
		Car car = carService.get(carRentHistoryDto.getCarRentId());
		Point pointFrom = pointService.get(carRentHistoryDto.getPointFromId());
		Point pointFinish = pointService.get(carRentHistoryDto.getPointFinishId());
		Renter renter = renterService.get(carRentHistoryDto.getRenterId());

		//save average days car + point
		CarTimeCounter carTimeCounter = carTimeCounterService.getCarAndPoint(car, pointFrom);

		if (carTimeCounter == null) {
			carTimeCounter = new CarTimeCounter();
			carTimeCounter.setCar(car);
			carTimeCounter.setPoint(pointFrom);
			carTimeCounter.setCounter(0L);
			carTimeCounter.setDays(0L);
		}

		carTimeCounter.setCounter(carTimeCounter.getCounter() + 1);
		long daysBetween = DAYS.between(carRentHistoryDto.getRentFrom(), carRentHistoryDto.getRentFinish());
		carTimeCounter.setDays(carTimeCounter.getDays() + daysBetween);
		carTimeCounterService.update(carTimeCounter);

		CarRentHistory carRentHistory = new CarRentHistory(car, pointFrom, pointFinish, renter, carRentHistoryDto.getRentFrom(), carRentHistoryDto.getRentFinish(), carTimeCounter);
		return create(carRentHistory);
	}

	@Transactional
	public CarRentHistory update(CarRentHistoryDto carRentHistoryDto) {
		CarRentHistory carRentHistory = get(carRentHistoryDto.getId());

		Car car = carService.get(carRentHistoryDto.getCarRentId());
		Point pointFrom = pointService.get(carRentHistoryDto.getPointFromId());
		Point pointFinish = pointService.get(carRentHistoryDto.getPointFinishId());
		Renter renter = renterService.get(carRentHistoryDto.getRenterId());

		carRentHistory.setCar(car);
		carRentHistory.setPointFrom(pointFrom);
		carRentHistory.setPointFinish(pointFinish);
		carRentHistory.setRenter(renter);
		carRentHistory.setRentFrom(carRentHistoryDto.getRentFrom());
		carRentHistory.setRentFinish(carRentHistoryDto.getRentFinish());
		return update(carRentHistory);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		CarRentHistory carRentHistory = get(id);

		CarTimeCounter carTimeCounter = carTimeCounterService.getCarAndPoint(carRentHistory.getCar(), carRentHistory.getPointFrom());

		if (carTimeCounter != null) {
			carTimeCounter.setCounter(carTimeCounter.getCounter() - 1);
			long daysBetween = DAYS.between(carRentHistory.getRentFinish(), carRentHistory.getRentFrom());
			carTimeCounter.setDays(carTimeCounter.getDays() - daysBetween);
			carTimeCounterService.update(carTimeCounter);
		}

		super.delete(id);
	}
}
