package com.ertelecom.carrent.service.impl;

import com.ertelecom.carrent.model.Car;
import com.ertelecom.carrent.repositories.CarRepository;
import com.ertelecom.carrent.service.CarService;
import org.springframework.stereotype.Service;

@Service
public class CarServiceImpl extends CrudImpl<CarRepository, Car> implements CarService {

	public CarServiceImpl(CarRepository repository) {
		super(repository);
	}
}
