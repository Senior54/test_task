package com.ertelecom.carrent.service;

import com.ertelecom.carrent.model.Point;
import org.springframework.stereotype.Service;

@Service
public interface PointService extends AbstractCrud<Point> {
}
