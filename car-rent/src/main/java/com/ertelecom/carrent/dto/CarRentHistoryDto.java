package com.ertelecom.carrent.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CarRentHistoryDto {
	private Long id;
	private Long carRentId;
	private Long pointFromId;
	private Long pointFinishId;
	private Long renterId;

	@ApiModelProperty(notes = "Time of start the rent", example = "yyyy-MM-dd", value = "0000-00-00")
	private LocalDate rentFrom;

	@ApiModelProperty(notes = "Time of finish the rent", example = "yyyy-MM-dd", value = "0000-00-00")
	private LocalDate rentFinish;
}