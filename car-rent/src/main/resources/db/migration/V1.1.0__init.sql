CREATE TABLE IF NOT EXISTS `cars`
(
    `id`     bigint(20) NOT NULL AUTO_INCREMENT,
    `name`   varchar(255) DEFAULT NULL,
    `number` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;

INSERT INTO `cars` (`id`, `name`, `number`)
VALUES (1, 'Toyota', 'O111OO777'),
       (2, 'Mercedes', 'O112OO777'),
       (3, 'Ford', 'O112OO777');

CREATE TABLE IF NOT EXISTS `points`
(
    `id`      bigint(20) NOT NULL AUTO_INCREMENT,
    `address` varchar(255) DEFAULT NULL,
    `city`    varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8;

INSERT INTO `points` (`id`, `address`, `city`)
VALUES (1, 'Lenina 1', 'Moscow'),
       (2, 'Lenina 2', 'Novosibirsk');

CREATE TABLE IF NOT EXISTS `renters`
(
    `id`   bigint(20) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8;

INSERT INTO `renters` (`id`, `name`)
VALUES (1, 'Anton Zenkoff'),
       (2, 'Some renter');

CREATE TABLE IF NOT EXISTS `cars_rent_history`
(
    `id`              bigint(20) NOT NULL AUTO_INCREMENT,
    `rent_finish`     date       DEFAULT NULL,
    `rent_from`       date       DEFAULT NULL,
    `car_id`          bigint(20) DEFAULT NULL,
    `point_finish_id` bigint(20) DEFAULT NULL,
    `point_from_id`   bigint(20) DEFAULT NULL,
    `renter_id`       bigint(20) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKlibcnyvhwkkhtnbuxf2kqir8n` (`car_id`),
    KEY `FKhy6sedh5wbuh3yfmqqoy7dobu` (`point_finish_id`),
    KEY `FKnjavk27nqa2o1kdctihp1y1s3` (`point_from_id`),
    KEY `FK9igbi4hg66rovkdhby8n6rh15` (`renter_id`),
    CONSTRAINT `FK9igbi4hg66rovkdhby8n6rh15` FOREIGN KEY (`renter_id`) REFERENCES `renters` (`id`),
    CONSTRAINT `FKhy6sedh5wbuh3yfmqqoy7dobu` FOREIGN KEY (`point_finish_id`) REFERENCES `points` (`id`),
    CONSTRAINT `FKlibcnyvhwkkhtnbuxf2kqir8n` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`),
    CONSTRAINT `FKnjavk27nqa2o1kdctihp1y1s3` FOREIGN KEY (`point_from_id`) REFERENCES `points` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;